#!/bin/bash --login
wget -t 5 https://gitlab.com/f2ce2b/resources/-/raw/master/%E6%96%87%E6%A1%A3%E6%A0%BC%E5%BC%8F/deb/%E9%B2%B2/kun_2022.12.15.1617.deb

if [[ $? -eq 0 ]];then 
  sudo dpkg --remove kun
else
  echo '[false, "安装包下载失败,请重试"]'
  exit 255
fi

sudo dpkg -i kun_2022.07.14.1507.deb
kun init
sudo kun reload
if [ -x "$(command -v kun)" ]; then rm -f kun_2022.07.14.1507.deb;fi
echo '[true, ""]'
